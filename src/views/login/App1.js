import React, {Component} from 'react';
import './App1.css';
import SignUp from './SignUp';
import SignIn from './SignIn';
import Overlay from './Overlay';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rightPanelActive: false,
        }
    }

    handleClickSignUpButton = () => this.setState({
        rightPanelActive: true,
    });

    handleClickSignInButton = () => this.setState({
        rightPanelActive: false,
    });


    render() {
        const { handleClickSignUpButton, handleClickSignInButton } = this;
        const { rightPanelActive } = this.state;
        const { onRouteChange, loadUser } = this.props;
        return (
            <div className="App">
                <div
                    className={`container ${rightPanelActive ? `right-panel-active` : ``}`}
                    id="container"
                >
                    <SignIn loadUser={loadUser} onRouteChange={onRouteChange}/>
                    <SignUp loadUser={loadUser} onRouteChange={onRouteChange}/>
                    <Overlay
                        handleClickSignInButton={handleClickSignInButton}
                        handleClickSignUpButton={handleClickSignUpButton}
                    />
                    
                </div>
            </div>
        );
    }
}

export default App;
