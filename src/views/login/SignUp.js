import React, { Component } from 'react';
import './App1.css';
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            regName: '',
            regEmail: '',
            regPass: ''
        }
    }
    onNameChange = (event) => {
        this.setState({regName: event.target.value})
    }
    onEmailChange = (event) => {
        this.setState({regEmail: event.target.value})
    }
    onPassChange = (event) => {
        this.setState({regPass: event.target.value})
    }

    onSubmitRegister = () => {
        fetch('http://localhost:8800/register', {
            method: 'post',
            headers: {'Content-Type' : 'application/json'},
            body: JSON.stringify({
                name: this.state.regName,
                email: this.state.regEmail,
                pass : this.state.regPass
            })
        }).then(response => response.json())
        .then(user => {
            if (user) {
                this.props.loadUser(user) // ini
                this.props.onRouteChange('home');
            }
        }).catch(err => {
            console.log(err)
        })
    }
    render() {
        return (
            <div className="form-container sign-up-container">
                <div className="form" >
                    <h1 className="form-title">Sign Up</h1>

                    <input onChange={this.onNameChange} className="input" type="text" placeholder="Name" />
                    <input onChange={this.onEmailChange} className="input" type="email" placeholder="Email Address" />
                    <input onChange={this.onPassChange} className="input" type="password" placeholder="Password" />
                    <button className="form-button" onClick={this.onSubmitRegister}>sign up</button>
                </div>
            </div>
        );
    }
}

export default SignUp;
