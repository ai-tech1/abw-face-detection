import React, { Component } from 'react';
import './App1.css';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signInEmail: '',
            signInPass: ''
        }
    }
    onEmailChange = (event) => {
        this.setState({signInEmail: event.target.value})
        console.log(this.state.signInEmail)
    }
    onPassChange = (event) => {
        this.setState({signInPass: event.target.value})
        console.log(this.state.signInPass)
    }

    // onSubmit = () => {
    //     fetch('http://localhost:8800/login', {
    //         method: 'post',
    //         headers: {'Content-Type' : 'application/json'},
    //         body: JSON.stringify({
    //             email: this.state.signInEmail,
    //             pass : this.state.signInPass
    //         })
    //     })
    //     .then(response => response.json())
    //     .then(user => {
    //         console.log(user)
    //         if (user[0] !== undefined) {
    //             this.props.onRouteChange('home')
    //             console.log(this.props)
    //         } else {
    //             console.log("username/pass salah")
    //         }
    //     })
    //     .catch(err => {
    //         // TODO nampilkan pass salah
    //         console.log(err)
    //         // console.log(this.props)
    //     })
    //     console.log("jancokkk!!!")
    // }

    onSubmitSignIn = () => {
        fetch('http://localhost:8800/login', {
                method: 'post',
                headers: { 'Content-Type': 'application/json'},
                body: JSON.stringify({
                    email: this.state.signInEmail,
                    pass: this.state.signInPass
                })
            })
            .then(response => response.json())
            .then(user => {
                console.log(user)
                if (user.id) {
                    this.props.loadUser(user) // ini
                    this.props.onRouteChange('home');
                }
            })
    }

    render() {
        return (
            <div className="form-container sign-in-container">
                <div className="form">
                    <h1 className="form-title">Welcome Back!</h1>
                    <input onChange={this.onEmailChange} className="input" type="email" placeholder="Email Address"/>
                    <input onChange={this.onPassChange} className="input" type="password" placeholder="Password" />
                    <button className="form-button" onClick={this.onSubmitSignIn}>Login</button>
                    <a className="find-password" href="http://www.google.com">lupa password ?</a>
                </div>
            </div>
        );
    }
}

export default SignIn;
