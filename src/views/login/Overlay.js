import React, { Component } from 'react';
import './App1.css';

interface Props {
    handleClickSignUpButton(event: any): void;
    handleClickSignInButton(event: any): void;
}

class Overlay extends Component<Props> {
    render() {
        const { handleClickSignUpButton, handleClickSignInButton } = this.props;
        return (
            <div className="overlay-container">
                <div className="overlay">
                    <div className="overlay-opacity">
                        <div className="overlay-panel overlay-left">
                            <h1>Welcome to AITECH!</h1>
                            <p className="overlay-description">
                            to keep connected with us please login,<br/>
                                with your personal info
                            </p>
                            <button
                                className="ghost form-button"
                                id="signIn"
                                onClick={handleClickSignInButton}
                            >Sign In</button>
                        </div>
                        <div className="overlay-panel overlay-right">
                            <h1>Welcome to AITECH</h1>
                            <p className="overlay-description">
                            Share your memorable photo with us <br/>
                            and we will tag the face in the photo
                            </p>
                            <button
                                className="ghost form-button"
                                id="signUp"
                                onClick={handleClickSignUpButton}
                            >Sign Up</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Overlay;