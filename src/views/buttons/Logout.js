import React from 'react';
import './button.css'

const Logout = ({onRouteChange}) => {
    return (
        <form action="/logout?_method=delete" method="post">
            <button className="login-button" onClick={()=>onRouteChange('signout')}>Logout</button>
        </form>
    )
}

export default Logout;