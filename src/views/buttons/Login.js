import React from 'react';
import './button.css';

const Login = ({onRouteChange}) => {
    return (
        <button className="login-button" onClick={() => onRouteChange('signin')}>Login</button>
    )
}

export default Login;