import React, { Component } from 'react';

class ImageLink extends Component {
    render() {
        const {onInputChange, onButtonSubmit, imgUrlsetState, login, nama, angka} = this.props;
        return (<div>
            <div className="content text-center">
                <div className="title">
                    <h1>
                        {login ? `Hello ${nama}!, You have submitted ${angka} images!` : `AI Face Detection!`}
                    </h1>
                    <p>This thing can detect faces as much as you want! So try yours now!</p>
                </div>
                <div className="get-div d-flex justify-content-center">
                    <input type="text" className="get-input " placeholder="image URL" onChange={onInputChange}/>
                    <button className="get-button btn" onClick={onButtonSubmit}>Scan!</button>
                </div>
                <h3>OR</h3>
                <div className="d-flex justify-content-center">
                    <label htmlFor="image" className="image-label d-flex justify-content-center align-items-center">Upload Yours!</label>
                    <input type="file" name="image" id="image" onChange={imgUrlsetState}/>
                </div>                             
            </div>
        </div>)
    }
}

export default ImageLink;