import React from 'react';

const FaceRecognition = ({imageUrl, boxes, textImage}) => {
    return (
        <div>
             <div className="result text-center">
                <h2>{textImage}</h2>
                <div className="position-absolute image ">
                    <img id='inputimage' width='240px' height='auto' src={imageUrl} alt=""/>
                    {boxes.map(box => {
                        return <div key={box.topRow} className='bounding-box' 
                                style={{top: box.topRow, right: box.rightCol, bottom: box.bottomRow, left: box.leftCol}}></div>
                    })
                    }
                </div>
            </div>
        </div>
    );
}

export default FaceRecognition;