import React, { Component } from 'react';
import { FaBars} from "react-icons/fa";
import Login from '../buttons/Login'
import Logout from '../buttons/Logout'

class Navbar extends Component {

    render() {  
        const {login, onRouteChange} = this.props;
         return (
        <div>
            <div className="navbar d-flex justify-content-between align-items-center">
                <div className="hamburger">
                    <FaBars size={20} color='white'/>
                </div>
                <div className="profile d-flex">
                    {
                        login ? <Logout onRouteChange={onRouteChange}/> : <Login onRouteChange={onRouteChange}/>
                    }
                </div>
            </div>
        </div>
    );
    }
}

export default Navbar;